package custom.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import custom.editor.clipboard.ClipboardObserver;
import custom.editor.edit.StackObserver;
import custom.editor.model.Location;
import custom.editor.model.LocationRange;
import custom.editor.model.TextEditorModel;
import custom.editor.model.observers.CursorObserver;
import custom.editor.model.observers.SelectionObserver;
import custom.editor.model.observers.TextObserver;
import custom.editor.plugins.Plugin;
import custom.editor.plugins.PluginFactory;

public class EditorFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private TextEditor editor;
	
	public EditorFrame(TextEditor editor) throws HeadlessException {
		this.editor = editor;
		
		setSize(500, 500);
		setLocationRelativeTo(null);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		initGUI();
	}


	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());

		add(editor, BorderLayout.CENTER);
		
		
		createMenuBar();
		createToolbar();
		createStatusBar();
		createActions();
	}
	
	private void createMenuBar() {
		JMenuBar mb = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		mb.add(fileMenu);
		
		fileMenu.add(new JMenuItem(open));
		fileMenu.add(new JMenuItem(save));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(exit));
		
		JMenu editMenu = new JMenu("Edit");
		mb.add(editMenu);
		
		editMenu.add(new JMenuItem(undo));
		editMenu.add(new JMenuItem(redo));
		editMenu.addSeparator();
		
		editMenu.add(new JMenuItem(cut));
		editMenu.add(new JMenuItem(copy));
		editMenu.add(new JMenuItem(paste));
		editMenu.add(new JMenuItem(pasteAndTake));
		editMenu.addSeparator();
		
		editMenu.add(new JMenuItem(delete));
		editMenu.add(new JMenuItem(clear));
		
		JMenu moveMenu = new JMenu("Move");
		mb.add(moveMenu);
		
		moveMenu.add(new JMenuItem(moveCursorToStart));
		moveMenu.add(new JMenuItem(moveCursorToEnd));
		
		JMenu pluginsMenu = new JMenu("Plugins");
		mb.add(pluginsMenu);
		
		List<Plugin> plugins = PluginFactory.newInstances();
		
		for (Plugin plugin : plugins) {
			Action pluginAction = new AbstractAction() {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					plugin.execute(editor.getModel(), editor.getManger(), editor.getClipboard());
					
					
				}
			};
			pluginAction.putValue(Action.NAME, plugin.getName());
			pluginAction.putValue(Action.SHORT_DESCRIPTION, plugin.getDescription());
			
			pluginsMenu.add(new JMenuItem(pluginAction));
		}
		setJMenuBar(mb);
	}


	private void createToolbar() {
		JToolBar tb = new JToolBar();
		tb.setFloatable(true);
		
		JButton undoButton = new JButton(undo);
		undoButton.setEnabled(false);
		tb.add(undoButton);
		
		JButton redoButton = new JButton(redo);
		redoButton.setEnabled(false);
		tb.add(redoButton);
		
		JButton cutButton = new JButton(cut);
		cutButton.setEnabled(false);
		tb.add(cutButton);
		
		JButton copyButton = new JButton(copy);
		copyButton.setEnabled(false);
		tb.add(copyButton);
		
		JButton pasteButton = new JButton(paste);
		pasteButton.setEnabled(false);
		tb.add(pasteButton);
			
		getContentPane().add(tb, BorderLayout.PAGE_START);
		
		editor.getClipboard().register(new ClipboardObserver() {
			
			@Override
			public void updateClipboard(boolean isEmpty) {
				pasteButton.setEnabled(!isEmpty);
			}
		});
		
		editor.getManger().registerUndoStackObserver(new StackObserver() {
			@Override
			public void stackUpdate(boolean isEmpty) {
				undoButton.setEnabled(!isEmpty);
			}
		});
		
		editor.getManger().registerRedoStackObserver(new StackObserver() {
			
			@Override
			public void stackUpdate(boolean isEmpty) {
				redoButton.setEnabled(!isEmpty);
			}
		});
		
		editor.registerSelectionObserver(new SelectionObserver() {
			
			@Override
			public void selectionUpdated(LocationRange selection) {
				if(selection == null) {
					copyButton.setEnabled(false);
					cutButton.setEnabled(false);
				} else {
					copyButton.setEnabled(true);
					cutButton.setEnabled(true);
				}
			}
		});
	}
	
	private void createStatusBar() {
		JPanel statusBar = new JPanel();
		statusBar.setLayout(new GridLayout(0, 2));
		statusBar.setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.GRAY));
		
		JLabel length = new JLabel();
		statusBar.add(length);
		length.setText("lines: " + editor.getLineCount());
		
		editor.registerTextObserver(new TextObserver() {
			
			@Override
			public void updateText() {
				length.setText("lines: " + editor.getLineCount());				
			}
		});
		
		length.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.GRAY));
		
		JLabel position = new JLabel();
		statusBar.add(position);

		editor.registerCursorObserver(new CursorObserver() {
			
			@Override
			public void updateCursorLocation(Location loc) {
				position.setText("Ln: " + loc.getY() + " Col: " + loc.getX());				
			}
		});
		
		position.setText("Ln: " + 0 + " Col: " + 0);
		position.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.GRAY));
		position.setOpaque(true);
		
		add(statusBar, BorderLayout.PAGE_END);
	}


	private void createActions() {
		open.putValue(Action.NAME,"Open");
		open.putValue(Action.SHORT_DESCRIPTION, "Open file from disk.");
		
		save.putValue(Action.NAME, "Save");
		save.putValue(Action.SHORT_DESCRIPTION, "Save file to disk.");

		exit.putValue(Action.NAME, "Exit");
		exit.putValue(Action.SHORT_DESCRIPTION, "Exit the application.");
		
		undo.putValue(Action.NAME,"Undo");
		undo.putValue(Action.SHORT_DESCRIPTION, "Undo the change.");
		
		redo.putValue(Action.NAME,"Redo");
		redo.putValue(Action.SHORT_DESCRIPTION, "Redo the change.");
		
		cut.putValue(Action.NAME,"Cut");
		cut.putValue(Action.SHORT_DESCRIPTION, "Cut selection.");
		
		copy.putValue(Action.NAME,"Copy");
		copy.putValue(Action.SHORT_DESCRIPTION, "Copy selection.");
		
		paste.putValue(Action.NAME,"Paste");
		paste.putValue(Action.SHORT_DESCRIPTION, "Paste from clipboard.");
		
		pasteAndTake.putValue(Action.NAME,"Paste and Take");
		pasteAndTake.putValue(Action.SHORT_DESCRIPTION, "Paste and remove from clipboard.");
		
		delete.putValue(Action.NAME, "Delete selection");
		delete.putValue(Action.SHORT_DESCRIPTION, "Delete selection.");
		
		clear.putValue(Action.NAME,"Clear document");
		clear.putValue(Action.SHORT_DESCRIPTION, "Clear the document.");
		
		moveCursorToStart.putValue(Action.NAME,"Cursor to document start");
		moveCursorToStart.putValue(Action.SHORT_DESCRIPTION, "Move cursor to the brginning of the document.");
		
		moveCursorToEnd.putValue(Action.NAME,"Cursor to document end");
		moveCursorToEnd.putValue(Action.SHORT_DESCRIPTION, "Move cursor to the end of the document.");
		
	}
	
	private final Action open = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Open file");
			if(jfc.showOpenDialog(EditorFrame.this) != JFileChooser.APPROVE_OPTION) {
				return;
			}

			Path source = jfc.getSelectedFile().toPath();
			String text = null;
			try {
				text = Files.readString(source);
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(
						EditorFrame.this,
						"An error occured while reading from file.",
						"Error",
						JOptionPane.ERROR_MESSAGE
						);
				return;
			}

			editor.setText(text);
		}
	};
	
	private Action save = new AbstractAction() {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Save file.");
			
			if(jfc.showSaveDialog(EditorFrame.this) != JFileChooser.APPROVE_OPTION) return;
			
			Path destination = jfc.getSelectedFile().toPath();
			try {
				Files.writeString(destination, editor.getText());
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(
						EditorFrame.this,
						"An error occured while saving the file.",
						"Error",
						JOptionPane.ERROR_MESSAGE
						);
				return;
			}
		}
	};
	
	
	private Action exit = new AbstractAction() {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	};
	
	private Action undo = new AbstractAction() {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.undo();
		}
	};

	private Action redo = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.redo();

		}
	};

	private Action cut = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.cut();
		}
	};

	private Action copy = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.copy();
		}
	};
	
	private Action paste = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.paste();
		}
	};
	
	private Action pasteAndTake = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.pasteAndPop();
		}
	};
	
	private Action delete = new AbstractAction() {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			editor.deleteSelection();
		}
	};
	
	private Action clear = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.clear();
		}
	};
	
	private Action moveCursorToStart = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.moveCursorToStart();
		}
	};
	
	private Action moveCursorToEnd = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editor.moveCursorToEnd();
		}
	};
	
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(() -> {
			TextEditorModel model = new TextEditorModel("Pocetni tekst.\nNovi redak");
			TextEditor editor = new TextEditor(model);
			
			EditorFrame frame = new EditorFrame(editor);
			
			frame.setVisible(true);
		});
	}
}
