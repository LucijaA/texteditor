package custom.editor.edit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

public class UndoManager {

	private static UndoManager manager = new UndoManager();
	
	private Stack<EditAction> undoStack;
	private Stack<EditAction> redoStack;
	private List<StackObserver> undoStackObservers;
	private List<StackObserver> redoStackObservers;
	
	private UndoManager() {
		super();
		this.undoStack = new Stack<>();
		this.redoStack = new Stack<>();
		this.undoStackObservers = new ArrayList<>();
		this.redoStackObservers = new ArrayList<>();
	}
	
	public void undo() {
		if(undoStack.isEmpty()) return;
		
		EditAction action = undoStack.pop();
		redoStack.push(action);
		
		action.execute_undo();

		notifyRedoStackObservers();
		notifyUndoStackObservers();
	}
	
	public void redo() {
		if(redoStack.isEmpty()) return;
		
		EditAction action = redoStack.pop();
		undoStack.push(action);
		
		action.execute_do();
		
		notifyRedoStackObservers();
		notifyUndoStackObservers();
	}
	
	public void push(EditAction c) {
		if(c == null) return;
		
		redoStack.clear();
		undoStack.push(c);
		
		notifyRedoStackObservers();
		notifyUndoStackObservers();
	}
	
	public void registerUndoStackObserver(StackObserver o) {
		undoStackObservers.add(Objects.requireNonNull(o));
	}
	
	public void unregisterUndoStackObserver(StackObserver o) {
		undoStackObservers.remove(o);
	}
	
	private void notifyUndoStackObservers() {
		undoStackObservers.forEach(o -> o.stackUpdate(undoStack.isEmpty()));
	}
	
	public void registerRedoStackObserver(StackObserver o) {
		redoStackObservers.add(Objects.requireNonNull(o));
	}
	
	public void unregisterRedoStackObserver(StackObserver o) {
		redoStackObservers.remove(o);
	}
	
	private void notifyRedoStackObservers() {
		redoStackObservers.forEach(o -> o.stackUpdate(redoStack.isEmpty()));
	}
	
	public static UndoManager getInstance() {
		return manager;
	}
}
