package custom.editor.edit;

public interface StackObserver {
	
	public void stackUpdate(boolean isEmpty);

}
