package custom.editor.model;
public class LocationRange {

	private Location start;
	private Location end;

	public LocationRange(Location start, Location end) {
		super();
		
		if(start.getY() > end.getY() || (start.getY() == end.getY() && start.getX() > end.getX())) {
			this.start = end;
			this.end = start;
		
		} else {
			this.start = start;
			this.end = end;
		}
	}

	public Location getStart() {
		return start;
	}
	public void setStart(Location start) {
		this.start = start;
	}
	public Location getEnd() {
		return end;
	}
	public void setEnd(Location end) {
		this.end = end;
	}
	
}
