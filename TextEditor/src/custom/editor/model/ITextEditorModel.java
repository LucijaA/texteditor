package custom.editor.model;
import java.util.Iterator;
import java.util.List;

import custom.editor.model.observers.CursorObserver;
import custom.editor.model.observers.SelectionObserver;
import custom.editor.model.observers.TextObserver;

public interface ITextEditorModel {

	public Iterator<String> allLines();

	public Iterator<String> linesRange(int index1, int index2);

	public void registerCursorObserver(CursorObserver o);

	public void unregisterCursorObserver(CursorObserver o);
	
	public void moveCursorLeft();
	
	public void moveCursorRight();
	
	public void moveCursorUp();
	
	public void moveCursorDown();

	public void registerTextObserver(TextObserver o);

	public void unregisterTextObserver(TextObserver o);
	
	public void deleteBefore();
	
	public void deleteAfter();
	
	public void deleteRange(LocationRange r);
	
	public LocationRange getSelectionRange();
	
	public void setSelectionRange(LocationRange r);
	
	public void insert(char c);
	
	public void insert(String text); 
	
	public void clear();

	public void moveCursorToStart();

	public void moveCursorToEnd();
	
	public void registerSelectionObserver(SelectionObserver o);

	public void unregisterSelectionObserver(SelectionObserver o);

	public int getLineCount();

	public List<String> getLines();

	public void notifyTextObservers();
}
