package custom.editor.model;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import custom.editor.edit.EditAction;
import custom.editor.edit.UndoManager;
import custom.editor.model.observers.CursorObserver;
import custom.editor.model.observers.SelectionObserver;
import custom.editor.model.observers.TextObserver;

public class TextEditorModel implements ITextEditorModel {

	private List<String> lines;
	private LocationRange selectionRange;
	private Location cursorLocation;
	private List<CursorObserver> cursorObservers;
	private List<TextObserver> textObservers;
	private List<SelectionObserver> selectionObservers;
	private UndoManager manager;
	private boolean createAction = true;
	
	
	public TextEditorModel(String text) {
		cursorObservers = new ArrayList<>();
		textObservers = new ArrayList<>();
		selectionObservers = new ArrayList<>();
		
		lines = new ArrayList<>();
		
		cursorLocation = new Location(0, 0);
		//selectionRange = new LocationRange(cursorLocation, cursorLocation);
		
		manager = UndoManager.getInstance();
		
		String[] parts = text.split("\\n");
		
		for (String line : parts) {
			lines.add(line);
		}
		
		notifyTextObservers();
		notifyCursorObservers();
		notifySelectionObservers();
	}
	
	@Override
	public Iterator<String> allLines() {
		return lines.iterator();
	}
	
	@Override
	public Iterator<String> linesRange(int index1, int index2) {
		return lines.subList(index1, index2).iterator();
	}

	@Override
	public void registerCursorObserver(CursorObserver o) {
		cursorObservers.add(
				Objects.requireNonNull(o)
				);
	}
	
	@Override
	public void unregisterCursorObserver(CursorObserver o) {
		cursorObservers.remove(o);
	}

	private void notifyCursorObservers() {
		for (CursorObserver cursorObserver : cursorObservers) {
			cursorObserver.updateCursorLocation(cursorLocation);
		}
	}

	@Override
	public void registerTextObserver(TextObserver o) {
		textObservers.add(
				Objects.requireNonNull(o)
				);
	}
	
	@Override
	public void unregisterTextObserver(TextObserver o) {
		textObservers.remove(o);
	}
	
	public void notifyTextObservers() {
		for (TextObserver textObserver : textObservers) {
			textObserver.updateText();
		}
	}

	@Override
	public void moveCursorLeft() {
		if(cursorLocation.getX() > 0) {
			cursorLocation.setX(cursorLocation.getX() - 1);
			notifyCursorObservers();
		} else if(cursorLocation.getY() > 0) {
			moveCursorUp();
			cursorLocation.setX(lines.get(cursorLocation.getY()).length());
		}
		
	}

	@Override
	public void moveCursorRight() {
		if(cursorLocation.getX() < lines.get(cursorLocation.getY()).length()) {
			cursorLocation.setX(cursorLocation.getX() + 1);
			notifyCursorObservers();
		
		} else if(cursorLocation.getY() < lines.size() - 1) {
			cursorLocation.setX(0);
			moveCursorDown();
		}
	}

	@Override
	public void moveCursorUp() {
		if(cursorLocation.getY() > 0) {
			cursorLocation.setY(cursorLocation.getY() - 1);
			
			if(cursorLocation.getX() > lines.get(cursorLocation.getY()).length()) {
				cursorLocation.setX(lines.get(cursorLocation.getY()).length());
			}
			notifyCursorObservers();
		}
	}

	@Override
	public void moveCursorDown() {
		if(cursorLocation.getY() < lines.size() - 1) {
			cursorLocation.setY(cursorLocation.getY() + 1);
			
			if(cursorLocation.getX() > lines.get(cursorLocation.getY()).length()) {
				cursorLocation.setX(lines.get(cursorLocation.getY()).length());
			}
			notifyCursorObservers();
		}	
	}

	@Override
	public void deleteBefore() {
		
		if(cursorLocation.getX() == 0 && cursorLocation.getY() != 0) {
			moveCursorLeft();
			
			if(createAction) pushDeleteBeforeAction( new Location(cursorLocation.getX(), cursorLocation.getY()), '\n');

			String secondLine = lines.remove(cursorLocation.getY() + 1);
			String firstLine = lines.remove(cursorLocation.getY());
			firstLine = firstLine.concat(secondLine);
			lines.add(cursorLocation.getY(), firstLine);
			
			notifyTextObservers();
		
		} else if(cursorLocation.getX() != 0) {
			String line = lines.remove(cursorLocation.getY());
			
			String newLine = line.substring(0, cursorLocation.getX() - 1)		
			.concat(
					line.substring(cursorLocation.getX())
					);
			lines.add(cursorLocation.getY(), newLine);
			
			moveCursorLeft();
			
			char c = line.charAt(cursorLocation.getX());
			Location oldCursor = new Location(cursorLocation.getX(), cursorLocation.getY());
			
			if(createAction) pushDeleteBeforeAction(oldCursor, c);
			
			notifyTextObservers();
		}
		
		
	}

	private void pushDeleteBeforeAction(Location oldCursor, char c) {
		manager.push(new EditAction() {
			
			@Override
			public void execute_undo() {
				createAction = false;
				cursorLocation = oldCursor;
				insert(c);
				createAction = true;
			}
			
			@Override
			public void execute_do() {
				createAction = false;
				cursorLocation = oldCursor;
				deleteBefore();
				createAction = true;
				
			}
		});
		
	}
	
	@Override
	public void deleteAfter() {
		if(cursorLocation.getX() == lines.get(cursorLocation.getY()).length() && cursorLocation.getY() != lines.size() - 1) {
			String currentLine = lines.get(cursorLocation.getY());
			String nextLine = lines.remove(cursorLocation.getY() + 1);
			currentLine = currentLine.concat(nextLine);
			
			if(createAction) pushDeleteAfterAction(new Location(cursorLocation.getX(), cursorLocation.getY()), '\n');
			
			notifyTextObservers();
		
		} else if(cursorLocation.getX() < lines.get(cursorLocation.getY()).length()) {
			String currentLine = lines.remove(cursorLocation.getY());
			String newLine = "";
			if(cursorLocation.getX() == 0) {
				newLine = currentLine.substring(1);
			
			} else {
				newLine = currentLine.substring(0, cursorLocation.getX())
						.concat(currentLine.substring(cursorLocation.getX() + 1));
								
			}
			lines.add(cursorLocation.getY(), newLine);
			
			char c = currentLine.charAt(cursorLocation.getX());
			if(createAction) pushDeleteAfterAction(new Location(cursorLocation.getX(), cursorLocation.getY()), c);
			
			notifyTextObservers();
		}
	}
	
	private void pushDeleteAfterAction(Location oldCursor, char c) {
		manager.push(new EditAction() {
			
			@Override
			public void execute_undo() {
				createAction = false;
				cursorLocation = oldCursor;
				insert(c);
				createAction = true;
			}
			
			@Override
			public void execute_do() {
				createAction = false;
				//Location saveCursor = cursorLocation;
				cursorLocation = oldCursor;
				deleteAfter();
				//cursorLocation = saveCursor;
				createAction = true;
			}
		});
	}

	@Override
	public void deleteRange(LocationRange r) {
		if(r == null) return;
		
		StringBuilder sb = new StringBuilder();
		
		LocationRange range = new LocationRange(
				new Location(r.getStart().getX(), r.getStart().getY()),
				new Location(r.getEnd().getX(), r.getEnd().getY())
				);
		
		if(r.getStart().getY() == r.getEnd().getY() && r.getStart().getX() < r.getEnd().getX()) {
			String currentLine = lines.remove(r.getStart().getY());
			String reducedLine = currentLine.substring(0, r.getStart().getX())
					.concat(
							currentLine.substring(r.getEnd().getX())
							);
			sb.append(currentLine.substring(r.getStart().getX(), r.getEnd().getX()));
			
			lines.add(r.getStart().getY(), reducedLine);
			
			setSelectionRange(null);
			
			cursorLocation.setX(r.getStart().getX());
			cursorLocation.setY(r.getStart().getY());
			

			if(createAction) pushDeleteRangeAction(range, sb.toString());
			
			notifyTextObservers();
			notifyCursorObservers();
		
		} else if(r.getStart().getY() < r.getEnd().getY()) {
			String currentLine = lines.remove(r.getStart().getY());
			currentLine = currentLine.substring(0, r.getStart().getX());
			
			sb.append(currentLine.substring(r.getStart().getX())).append("\n");
			
			int linesToBeDeleted = r.getEnd().getY() - r.getStart().getY();
			String lastLine = "";
			
			while(linesToBeDeleted > 0) {
				lastLine = lines.remove(r.getStart().getY());
				linesToBeDeleted--;
				
				if(linesToBeDeleted > 0) sb.append(lastLine).append("\n");
			}
			
			if(lastLine.length() > r.getEnd().getX()) {
				currentLine = currentLine.concat(lastLine.substring(r.getEnd().getX()));
				sb.append(lastLine.substring(0, r.getEnd().getX()));
			} else {
				sb.append(lastLine);
			}
			
			lines.add(r.getStart().getY(), currentLine);
			
			setSelectionRange(null);
			cursorLocation.setX(r.getStart().getX());
			cursorLocation.setY(r.getStart().getY());
			
			if(createAction) pushDeleteRangeAction(range, sb.toString());
			
			notifyTextObservers();
			notifyCursorObservers();
		}
	}
	
	private void pushDeleteRangeAction(LocationRange range, String text) {
		manager.push(new EditAction() {
			
			@Override
			public void execute_undo() {
				createAction = false;
				cursorLocation.setX(range.getStart().getX());
				cursorLocation.setY(range.getStart().getY());
				
				insert(text);
				createAction = true;
				
			}
			
			@Override
			public void execute_do() {
				createAction = false;
				cursorLocation.setX(range.getStart().getX());
				cursorLocation.setY(range.getStart().getY());
				deleteRange(range);
				createAction = true;
			}
		});
	}

	@Override
	public LocationRange getSelectionRange() {
		return selectionRange;
	}

	@Override
	public void setSelectionRange(LocationRange r) {
		this.selectionRange = r;
		notifySelectionObservers();
	}

	@Override
	public void insert(char c) {
		
		if(c == '\n') {
			String line = lines.remove(cursorLocation.getY());
			String firstPart = line.substring(0, cursorLocation.getX());
			String secondPart = line.substring(cursorLocation.getX());
			
			lines.add(cursorLocation.getY(), secondPart);
			lines.add(cursorLocation.getY(), firstPart);
			
			moveCursorDown();			
			cursorLocation.setX(0);
		} else {
			
			String line = lines.remove(cursorLocation.getY());
			StringBuilder sb = new StringBuilder(line);
			sb.insert(cursorLocation.getX(), c);
			
			lines.add(cursorLocation.getY(), sb.toString());
			moveCursorRight();
		}
		
		Location cursor = new Location(cursorLocation.getX(), cursorLocation.getY());
		if(createAction) pushInsertAction(cursor, c);
		
		notifyCursorObservers();
		notifyTextObservers();
	}
	
	private void pushInsertAction(Location oldCursor, char c) {
		manager.push(new EditAction() {
			
			@Override
			public void execute_undo() {
				createAction = false;
				cursorLocation = oldCursor;
				deleteBefore();
				createAction = true;
			}
			
			@Override
			public void execute_do() {
				createAction = false;
				cursorLocation = oldCursor;
				insert(c);
				createAction = true;
			}
		});
	}

	@Override
	public void insert(String text) {
		Stream<String> lines = text.lines();
		
		Iterator<String> it = lines.iterator();
		
		Location start = new Location(cursorLocation.getX(), cursorLocation.getY());
		
		//concatenate first line
		String currentLine = this.lines.remove(cursorLocation.getY());
		String firstPart = currentLine.substring(0, cursorLocation.getX());
		String secondPart = currentLine.substring(cursorLocation.getX());
		
		if(it.hasNext()) {
			String line = it.next();
			currentLine = firstPart.concat(line);
			
			if(it.hasNext()) this.lines.add(cursorLocation.getY(), currentLine);
			else this.lines.add(cursorLocation.getY(), currentLine.concat(secondPart));
			cursorLocation.setX(currentLine.length());
		}
		
		//add the rest of lines if any
		while(it.hasNext()) {
			String line = it.next();
			if(it.hasNext()) this.lines.add(cursorLocation.getY() + 1, line);
			else this.lines.add(cursorLocation.getY() + 1, line.concat(secondPart));
			
			moveCursorDown();
			cursorLocation.setX(line.length());
		}
		
		Location end = new Location(cursorLocation.getX(), cursorLocation.getY());
		
		if(createAction) pushInsertAction(new LocationRange(start, end), text);
		
		notifyCursorObservers();
		notifyTextObservers();
	}
	
	private void pushInsertAction(LocationRange range, String text) {
		manager.push(new EditAction() {
			
			@Override
			public void execute_undo() {
				createAction = false;
				cursorLocation = range.getStart();
				deleteRange(range);
				createAction = true;
			}
			
			@Override
			public void execute_do() {
				createAction = false;
				cursorLocation = range.getStart();
				insert(text);
				createAction = true;
			}
		});
	}

	@Override
	public void clear() {
		Iterator<String> it = lines.iterator();
		
		while(it.hasNext()) {
			it.next();
			it.remove();
		}
		moveCursorToStart();
	
		notifyTextObservers();
	}

	@Override
	public void moveCursorToStart() {
		cursorLocation.setX(0);
		cursorLocation.setY(0);
		
		notifyCursorObservers();
	}

	@Override
	public void moveCursorToEnd() {
		cursorLocation.setX(lines.get(lines.size() - 1).length());
		cursorLocation.setY(lines.size() - 1);
		
		notifyCursorObservers();
	}

	@Override
	public void registerSelectionObserver(SelectionObserver o) {
		selectionObservers.add(Objects.requireNonNull(o));
		
	}

	@Override
	public void unregisterSelectionObserver(SelectionObserver o) {
		selectionObservers.remove(o);
		
	}
	
	private void notifySelectionObservers() {
		selectionObservers.forEach(o -> o.selectionUpdated(selectionRange));
	}

	@Override
	public int getLineCount() {
		return this.lines.size();
	}

	@Override
	public List<String> getLines() {
		return this.lines;
	}
}
