package custom.editor.model.observers;

import custom.editor.model.LocationRange;

public interface SelectionObserver {
	
	public void selectionUpdated(LocationRange selection);

}
