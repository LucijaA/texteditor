package custom.editor.model.observers;

import custom.editor.model.Location;

public interface CursorObserver {

	public void updateCursorLocation(Location loc);
}
