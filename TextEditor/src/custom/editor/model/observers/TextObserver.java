package custom.editor.model.observers;

public interface TextObserver {

	public void updateText();
}
