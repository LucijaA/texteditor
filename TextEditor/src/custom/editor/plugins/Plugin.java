package custom.editor.plugins;

import custom.editor.clipboard.ClipboardStack;
import custom.editor.edit.UndoManager;
import custom.editor.model.ITextEditorModel;

public interface Plugin {
	
	public String getName();
	
	public String getDescription();
	
	public void execute(ITextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack);
}