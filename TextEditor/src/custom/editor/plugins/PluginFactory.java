package custom.editor.plugins;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PluginFactory {

	@SuppressWarnings("unchecked")
	public static List<Plugin> newInstances() {
		List<String> pluginNames = Arrays.asList("Statistika", "VelikoSlovo");
		List<Plugin> plugins = new ArrayList<>();

		for (String name : pluginNames) {
			
			try {
			Class<Plugin> plugin = (Class<Plugin>)Class.forName("custom.editor.plugins.impl." + name);
			Constructor<?> ctr;
				ctr = plugin.getConstructor();
				plugins.add((Plugin)ctr.newInstance());			
			} catch (NoSuchMethodException | InvocationTargetException | InstantiationException 
					| IllegalAccessException | SecurityException | ClassNotFoundException e) {}
		}
		return plugins;
	}
}
