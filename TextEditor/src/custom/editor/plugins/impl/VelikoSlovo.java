package custom.editor.plugins.impl;
import java.util.List;

import custom.editor.clipboard.ClipboardStack;
import custom.editor.edit.UndoManager;
import custom.editor.model.ITextEditorModel;
import custom.editor.plugins.Plugin;

public class VelikoSlovo implements Plugin {

	@Override
	public String getName() {
		return "Upper-case letter";
	}

	@Override
	public String getDescription() {
		return "Turns every starting letter of the word to upper-case.";
	}

	@Override
	public void execute(ITextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
		List<String> lines = model.getLines();
		
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.remove(i);
			String[] parts = line.split("\\s+");
			
			StringBuilder sb = new StringBuilder();
			
			for (int j = 0; j < parts.length; j++) {
				if(parts[j].length() > 1) {
					String firstLetter = parts[j].substring(0, 1).toUpperCase();
					String word = parts[j].substring(1);
					sb.append(firstLetter).append(word).append(" ");
				} else {
					sb.append(parts[j].toUpperCase()).append(" ");
				}
			}
			
			String newLine = sb.toString().trim();
			lines.add(i, newLine);
			
			model.notifyTextObservers();
		}
	}

}
