package custom.editor.plugins.impl;

import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import custom.editor.clipboard.ClipboardStack;
import custom.editor.edit.UndoManager;
import custom.editor.model.ITextEditorModel;
import custom.editor.plugins.Plugin;


public class Statistika extends JComponent implements Plugin {

	private static final long serialVersionUID = 1L;
	private String name = "Statistics";
	private String description = "Shows number of lines, words and letters.";
	private int lineCount = 0;
	private int wordCount = 0;
	private int letterCount = 0;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void execute(ITextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
		Iterator<String> it = model.allLines();
		
		while(it.hasNext()) {
			lineCount++;
			String[] lineParts = it.next().split("\\s+");
			
			wordCount += lineParts.length;
			
			for (int i = 0; i < lineParts.length; i++) {
				letterCount += lineParts[i].length();
			}
		}
		
		JOptionPane.showMessageDialog(
				this,
				"broj linija: " + lineCount + " broj rijeci: " + wordCount + " broj slova: " + letterCount);

		
	}

}
