package custom.editor;
import static java.lang.Character.isAlphabetic;
import static java.lang.Character.isDigit;
import static java.lang.Character.isWhitespace;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.swing.JComponent;

import custom.editor.clipboard.ClipboardStack;
import custom.editor.edit.UndoManager;
import custom.editor.model.ITextEditorModel;
import custom.editor.model.Location;
import custom.editor.model.LocationRange;
import custom.editor.model.TextEditorModel;
import custom.editor.model.observers.CursorObserver;
import custom.editor.model.observers.SelectionObserver;
import custom.editor.model.observers.TextObserver;

public class TextEditor extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ITextEditorModel model;
	private Location cursorLocation;
	private Location selectionBeginning;
	private boolean isShiftPressed = false;
	private boolean isControlPressed = false;
	private ClipboardStack clipboard;
	private UndoManager manager;
	
	public TextEditor(TextEditorModel model) {
		super();
		this.model = Objects.requireNonNull(model);
		this.cursorLocation = new Location(0, 0);
		this.selectionBeginning = new Location(0, 0);
		this.clipboard = new ClipboardStack();
		this.manager = UndoManager.getInstance();
		
		model.registerCursorObserver(new CursorObserver() {
			
			@Override
			public void updateCursorLocation(Location loc) {
				cursorLocation = loc;
				repaint();
			}
		});
		
		model.registerTextObserver(new TextObserver() {
			
			@Override
			public void updateText() {
				repaint();
				
			}
		});
		setFocusable(true);

		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {

				switch(e.getKeyCode()) {
				case KeyEvent.VK_UP:					
					model.moveCursorUp();
					
					if(isShiftPressed) {
						model.setSelectionRange(new LocationRange(selectionBeginning, cursorLocation));
					} else {
						model.setSelectionRange(null);
					}
					
					e.consume();
					break;
					
				case KeyEvent.VK_DOWN:
					model.moveCursorDown();
					
					if(isShiftPressed) {
						model.setSelectionRange(new LocationRange(selectionBeginning, cursorLocation));
					} else {
						model.setSelectionRange(null);
					}
					
					e.consume();
					break;
					
				case KeyEvent.VK_LEFT:
					model.moveCursorLeft();
					
					if(isShiftPressed) {
						model.setSelectionRange(new LocationRange(selectionBeginning, cursorLocation));
					}  else {
						model.setSelectionRange(null);
					}
					
					e.consume();
					break;
					
				case KeyEvent.VK_RIGHT:
					model.moveCursorRight();
					
					if(isShiftPressed) {
						model.setSelectionRange(new LocationRange(selectionBeginning, cursorLocation));
					} else {
						model.setSelectionRange(null);
					}
					
					e.consume();
					break;
				
				case KeyEvent.VK_SHIFT:
					if(!isShiftPressed) {						
						isShiftPressed = true;
						selectionBeginning = model.getSelectionRange() == null ? 
								new Location(cursorLocation.getX(), cursorLocation.getY()) : selectionBeginning;
					}
					e.consume();
					break;
				
				case KeyEvent.VK_BACK_SPACE:
					if(model.getSelectionRange() == null) model.deleteBefore();
					else model.deleteRange(model.getSelectionRange());
					
					e.consume();
					break;
					
				case KeyEvent.VK_DELETE:
					if(model.getSelectionRange() == null) model.deleteAfter();
					else model.deleteRange(model.getSelectionRange());
					
					e.consume();
					break;
				
				case KeyEvent.VK_CONTROL:
					isControlPressed = true;
					e.consume();
					break;
				
				case KeyEvent.VK_C:
					if(isControlPressed) {
						copy();	
					}
					e.consume();
					break;
				
				case KeyEvent.VK_X:
					if(isControlPressed) {
						cut();
					}
					e.consume();
					break;
					
				case KeyEvent.VK_V:
					if(isControlPressed) {
						if(isShiftPressed) {
							pasteAndPop();
						} else {
							paste();
						}
					}
					e.consume();
					break;
				
				case KeyEvent.VK_Z:
					if(isControlPressed) {
						manager.undo();
					}
					e.consume();
					break;
				
				case KeyEvent.VK_Y:
					if (isControlPressed) {
						manager.redo();
					}
					e.consume();
					break;
				}
			}
			

			@Override
			public void keyReleased(KeyEvent e) {
				switch(e.getKeyCode()) {
				case KeyEvent.VK_SHIFT:
					isShiftPressed = false;
					e.consume();
					break;

				case KeyEvent.VK_CONTROL:
					isControlPressed = false;
					e.consume();
					break;
				}
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				
				if(isAlphabetic(c) || isDigit(c) || isWhitespace(c) || Pattern.matches("\\p{Punct}", "" + c)) {
					if(model.getSelectionRange() != null) model.deleteRange(model.getSelectionRange());					
					model.insert(e.getKeyChar());
					e.consume();
				}
			}
		});
	}
	
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		
		int fontHeight = g2d.getFontMetrics().getHeight();

		if(model.getSelectionRange() != null) { //change selection background color

			LocationRange range = model.getSelectionRange();

			Color selection = Color.CYAN;
			Color background = g2d.getBackground();

			if(background.equals(selection)) {
				selection = Color.GREEN;
			}

			g2d.setColor(selection);

			int numberOfLines = range.getEnd().getY() - range.getStart().getY();
			int startX = getPosition(g2d.getFontMetrics(), range.getStart().getY(), 0, range.getStart().getX());

			if(numberOfLines == 0) {
				int lenX = getPosition(g2d.getFontMetrics(), range.getStart().getY(), range.getStart().getX(), range.getEnd().getX());
				
				g2d.fillRect(startX, range.getStart().getY() * fontHeight, lenX, fontHeight);

			} else {
				int lenX = getPosition(g2d.getFontMetrics(), range.getStart().getY(), range.getStart().getX(), -1);
				
				g2d.fillRect(startX, range.getStart().getY() * fontHeight, lenX, fontHeight); //beginning line
				
				while (numberOfLines > 1) {
					int xLen =getPosition(g2d.getFontMetrics(), range.getEnd().getY() - numberOfLines + 1, 0, -1);
					
					g2d.fillRect(0, (range.getEnd().getY() - numberOfLines + 1)* fontHeight, xLen, fontHeight); //full lines
						
					numberOfLines--;
				}
				
				lenX = getPosition(g2d.getFontMetrics(), range.getEnd().getY(), 0, range.getEnd().getX());
				
				g2d.fillRect(0, range.getEnd().getY() * fontHeight, lenX, fontHeight);
			}
		}
		
		g2d.setColor(Color.BLACK);
		
		int y = fontHeight;
		for (Iterator<String> iterator = model.allLines(); iterator.hasNext();) {
			g2d.drawString(iterator.next(), 0, y);
			y += fontHeight;
		}
		
		int position = getPosition(g2d.getFontMetrics(), cursorLocation.getY(), 0, cursorLocation.getX());
		
		g2d.drawLine(
				position,
				cursorLocation.getY()*fontHeight,
				position,
				cursorLocation.getY()*fontHeight + fontHeight
				); //draw cursor
		
		
	}
	
	private int getPosition(FontMetrics fontMetrics, int lineIndex, int start, int end) {

		Iterator<String> it = model.linesRange(lineIndex, lineIndex + 1);
		if(it.hasNext()) {
			String line = it.next();
			if(end == -1) end = line.length();
			
			String beforeCursor = line.substring(start, end);

			return fontMetrics.stringWidth(beforeCursor);
		}
		return 0;
	}
	
	private String getTextInRange(LocationRange selectionRange) {
		Location start = model.getSelectionRange().getStart();
		Location end = model.getSelectionRange().getEnd();
		
		Iterator<String> it = model.linesRange(start.getY(), end.getY() + 1);
		StringBuilder sb = new StringBuilder();
		
		if(it.hasNext()) {
			String line = it.next();
			
			if(it.hasNext()) sb.append(line.substring(start.getX())).append("\n");
			else {
				sb.append(line.substring(start.getX(), end.getX()));
				return sb.toString();
			}
		}
		
		while(it.hasNext()) {
			String line = it.next();
			
			if(!it.hasNext()) {
				sb.append(line.substring(0, end.getX()));
				break;
			}
			sb.append(line).append("\n");
		}
		
		return sb.toString();
	}

	public String getText() {
		StringBuilder sb = new StringBuilder();

		Iterator<String> it = model.allLines();
		
		while(it.hasNext()) {
			sb.append(it.next()).append('\n');
		}
		
		return sb.toString();
	}

	public void setText(String text) {
		model.clear();
		model.insert(text);
	}

	public void undo() {
		manager.undo();
	}

	public void redo() {
		manager.redo();
	}

	public void cut() {
		if(model.getSelectionRange() != null) {
			String text = getTextInRange(model.getSelectionRange());
			clipboard.push(text);
			model.deleteRange(model.getSelectionRange());
		}
		
	}

	public void copy() {
		if(model.getSelectionRange() != null) {
			String text = getTextInRange(model.getSelectionRange());
			clipboard.push(text);
		}
	}

	public void paste() {
		if(!clipboard.isEmpty()) {
			String text = clipboard.peek();
			if(model.getSelectionRange() != null) model.deleteRange(model.getSelectionRange());
			
			model.insert(text);
		}
	}

	public void pasteAndPop() {
		if(!clipboard.isEmpty()) {
			String text = clipboard.pop();
			if(model.getSelectionRange() != null) model.deleteRange(model.getSelectionRange());
			
			model.insert(text);
		}
	}

	public void deleteSelection() {
		if(model.getSelectionRange() != null) {
			model.deleteRange(model.getSelectionRange());
		}
	}

	public void clear() {
		this.model.clear();
	}

	public void moveCursorToStart() {
		model.moveCursorToStart();
	}

	public void moveCursorToEnd() {
		model.moveCursorToEnd();
	}
	
	public ClipboardStack getClipboard() {
		return clipboard;
	}
	
	public UndoManager getManger() {
		return manager;
	}
	
	public void registerSelectionObserver(SelectionObserver o) {
		model.registerSelectionObserver(o);
	}
	
	public void unregisterSelectionObserver(SelectionObserver o) {
		model.unregisterSelectionObserver(o);
	}

	public int getLineCount() {
		return model.getLineCount();
	}

	public void registerTextObserver(TextObserver o) {
		model.registerTextObserver(o);
	}
	
	public void unregisterTextObserver(TextObserver o) {
		model.unregisterTextObserver(o);
	}

	public void registerCursorObserver(CursorObserver o) {
		model.registerCursorObserver(o);
	}
	
	public void unregisterCursorObserver(CursorObserver o) {
		model.unregisterCursorObserver(o);
	}

	public ITextEditorModel getModel() {
		return model;
	}
}
