package custom.editor.clipboard;


public interface ClipboardObserver {
	
	public void updateClipboard(boolean isEmpty);
	
}
