package custom.editor.clipboard;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

public class ClipboardStack {

	private Stack<String> texts = new Stack<>();
	private List<ClipboardObserver> observers = new ArrayList<>();

	public boolean isEmpty() {
		return texts.isEmpty();
	}

	public int size() {
		return texts.size();
	}

	public void push(String value) {
		texts.push(value);
		notifyObservers();
	}

	public String pop() {
		if(isEmpty()) throw new EmptyStackException();
		
		String element = texts.pop();
		notifyObservers();
		
		return element;
	}

	public String peek() {
		if(isEmpty()) throw new EmptyStackException();

		return texts.peek();
	}

	public void clear() {
		texts.clear();
		notifyObservers();
	}
	
	public void register(ClipboardObserver o) {
		observers.add(Objects.requireNonNull(o));
	}
	
	public void unregister(ClipboardObserver o) {
		observers.remove(o);
	}
	
	private void notifyObservers() {
		observers.forEach(o -> o.updateClipboard(isEmpty()));
	}
}
